const express = require('express')
const router = express.Router()

const getProducts = function (req, res, next) {
  res.json([
    { id: 1, name: 'IPad gen1 64G wifi', price: 11000.0 },
    { id: 2, name: 'IPad gen2 64G wifi', price: 12000.0 },
    { id: 3, name: 'IPad gen3 64G wifi', price: 13000.0 },
    { id: 4, name: 'IPad gen4 64G wifi', price: 14000.0 },
    { id: 5, name: 'IPad gen5 64G wifi', price: 15000.0 },
    { id: 6, name: 'IPad gen6 64G wifi', price: 16000.0 },
    { id: 7, name: 'IPad gen7 64G wifi', price: 17000.0 },
    { id: 8, name: 'IPad gen8 64G wifi', price: 18000.0 },
    { id: 9, name: 'IPad gen9 64G wifi', price: 19000.0 },
    { id: 10, name: 'IPad gen10 64G wifi', price: 20000.0 }
  ])
}

router.get('/', getProducts)

module.exports = router
