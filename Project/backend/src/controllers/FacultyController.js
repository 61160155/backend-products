const res = require('express/lib/response')
const dbConn = require('../../config/db.config')
const FacultyModel = require('../models/Faculty')

// get all Faculty
exports.getAllFaculty = (req, res) => {
  //console.log('Get faculty successfully')
  FacultyModel.getAllFaculty((err, faculty) => {
    console.log('Get all facultys success fully')
    if(err)
    res.send(err)
    console.log('facultys', faculty)
    res.send(faculty)   
  })
}
exports.getFacultyById = (req, res) => {
  FacultyModel.getFacultyById(req.params.id, (err, faculty) => {
    console.log('Get faculty by id successfully')
    if(err)
    res.send(err)
    console.log('Faculty', faculty);
    res.send(faculty)
  })
}
//add Faculty
exports.createNewFaculty = (req, res) => {
  console.log('Req faculty data', req.body)
  const facultyReqData = new FacultyModel(req.body)

  if(req.body.contructor === Object && Object.keys(req.body).length === 0) {
    res.send(400).send({success: false, message: 'Please fill all fields', faculty})
  }else {
    console.log('Valid data')
    FacultyModel.createNewFaculty(facultyReqData, (err, faculty) => {
      if(err)
      res.stutus(500).send(err)
      res.json({ status: true, message: 'Insert faculty completed', data: faculty })
    })
  }
}
// update Faculty
exports.updateFaculty = (req, res) => {
  const facultyReqData = new FacultyModel(req.body)
  console.log('faculty req data update', facultyReqData)

  if(req.body.contructor === Object && Object.keys(req.body).length === 0) {
    res.send(400).send({success: false, message: 'Please fill all fields'})
  }else {
    console.log('Valid data')
    FacultyModel.updateFaculty(req.params.id, facultyReqData, (err, faculty) => {
      if(err)
      res.send(err)
      res.json({ status : true, message: 'Update faculty information successfully', data: faculty})
    })
  }
}

// delelte Faculty
exports.deleteFaculty = (req, res) => {
  FacultyModel.deleteFaculty(req.params.id, (err, faculty) => {
    if(err)
    res.send(err)
    res.json({ success: true, message: 'Deleted faculty successfully'})
  })
}