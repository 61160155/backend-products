const res = require('express/lib/response')
const dbConn = require('../../config/db.config')
const ApplyModel = require('../models/Apply')

// get all subjects
exports.getAllApply = (req, res) => {
  //console.log('Get user successfully')
  ApplyModel.getAllApply((err, apply) => {
    console.log('Get all applys success fully')
    if(err)
    res.send(err)
    console.log('applys', apply)
    res.send(apply)   
  })
}
exports.getApplyById = (req, res) => {
  ApplyModel.getApplyById(req.params.id, (err, apply) => {
    console.log('Get apply by id successfully')
    if(err)
    res.send(err)
    console.log('Apply', apply);
    res.send(apply)
  })
}
//add
exports.createNewApply = (req, res) => {
  console.log('Req apply data', req.body)
  const memberReqData = new ApplyModel(req.body)

  if(req.body.contructor === Object && Object.keys(req.body).length === 0) {
    res.send(400).send({success: false, message: 'Please fill all fields', apply})
  }else {
    console.log('Valid data')
    ApplyModel.createNewApply(memberReqData, (err, member) => {
      if(err)
      res.stutus(500).send(err)
      res.json({ status: true, message: 'Insert apply completed', data: member })
    })
  }
}
// update apply
exports.updateApply = (req, res) => {
  const memberReqData = new ApplyModel(req.body)
  console.log('applyt req data update', memberReqData)

  if(req.body.contructor === Object && Object.keys(req.body).length === 0) {
    res.send(400).send({success: false, message: 'Please fill all fields'})
  }else {
    console.log('Valid data')
    ApplyModel.updateApply(req.params.id, memberReqData, (err, member) => {
      if(err)
      res.send(err)
      res.json({ status : true, message: 'Update apply information successfully', data: member})
    })
  }
}

// delelte subject
exports.deleteApply = (req, res) => {
  ApplyModel.deleteApply(req.params.id, (err, apply) => {
    if(err)
    res.send(err)
    res.json({ success: true, message: 'Deleted apply successfully'})
  })
}