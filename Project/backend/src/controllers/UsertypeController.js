const res = require('express/lib/response')
const dbConn = require('../../config/db.config')
const UsertypeModel = require('../models/Usertype')

// get all subjects
exports.getAllUsertype = (req, res) => {
  //console.log('Get user successfully')
  UsertypeModel.getAllUsertype((err, usertype) => {
    console.log('Get all usertypes success fully')
    if(err)
    res.send(err)
    console.log('usertypes', usertype)
    res.send(usertype)   
  })
}
exports.getUsertypeById = (req, res) => {
  UsertypeModel.getUsertypeById(req.params.id, (err, usertype) => {
    console.log('Get usertype by id successfully')
    if(err)
    res.send(err)
    console.log('Usertype', usertype);
    res.send(usertype)
  })
}
//add
exports.createNewUsertype = (req, res) => {
  console.log('Req usertype data', req.body)
  const usertypeReqData = new UsertypeModel(req.body)

  if(req.body.contructor === Object && Object.keys(req.body).length === 0) {
    res.send(400).send({success: false, message: 'Please fill all fields', usertype})
  }else {
    console.log('Valid data')
    UsertypeModel.createNewUsertype(usertypeReqData, (err, usertype) => {
      if(err)
      res.stutus(500).send(err)
      res.json({ status: true, message: 'Insert usertype completed', data: usertype })
    })
  }
}
// update Usertype
exports.updateUsertype = (req, res) => {
  const usertypeReqData = new UsertypeModel(req.body)
  console.log('usertype req data update', usertypeReqData)

  if(req.body.contructor === Object && Object.keys(req.body).length === 0) {
    res.send(400).send({success: false, message: 'Please fill all fields'})
  }else {
    console.log('Valid data')
    UsertypeModel.updateUsertype(req.params.id, usertypeReqData, (err, usertype) => {
      if(err)
      res.send(err)
      res.json({ status : true, message: 'Update usertype information successfully', data: usertype})
    })
  }
}

// delelte subject
exports.deleteUsertype = (req, res) => {
  UsertypeModel.deleteUsertype(req.params.id, (err, usertype) => {
    if(err)
    res.send(err)
    res.json({ success: true, message: 'Deleted usertype successfully'})
  })
}