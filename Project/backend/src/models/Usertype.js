
var dbCon = require('../../config/db.config')

var Usertype = function(usertype) {
  this.id = usertype.id
  this.u_type = usertype.u_type
}
// get all Usertype
Usertype.getAllUsertype = (result) => {
  dbCon.query('SELECT * FROM usertype', (err, res) => {
    if(err) {
      console.log('Error while fetching usertype', err)
      result(null, err)
    }else {
      console.log('usertype fetched successfully')
      result(null, res)
    }
  })
}
// get Usertype by id 
Usertype.getUsertypeById = (id, result) => {
  dbCon.query('SELECT * FROM usertype WHERE id=?', id, (err, res) => {
    if(err) {
      console.log('Error while fetching usertype by id', err)
      result(null,err)
    }else {
      console.log('usertype fetched by id successfully')
      result(null,res)
    }
  })
}
// insert usertype
Usertype.createNewUsertype = (usertypeReqData, result) => {
  dbCon.query('INSERT INTO usertype SET ?', usertypeReqData, (err, res) => {
    if(err) {
      console.log('Error while inserting data')
      result(null,err)
    }else {
      console.log('Insert new usertype successfully')
      result(null, res)
    }
  })
}
// update Usertype
Usertype.updateUsertype = (id, usertypeReqData, result) => {
  dbCon.query('UPDATE usertype SET u_type=? WHERE id=?', [usertypeReqData.u_type, id] , (err, res) => {
    if(err) {
      console.log('Error while update data')
      result(null, err)
    } else {
      console.log('Update usertype successfully')
      result(null, res)
    }
  })
}
// delete Usertype
Usertype.deleteUsertype = (id, result) => {
  dbCon.query('DELETE FROM usertype WHERE id=?', [id], (err, res) => {
    if(err) {
      console.log('Error while deleting usertype')
      result(null, err)
    }else {
      console.log('Delete usertype successfully')
      result(null, res)
    }
  })
}
module.exports = Usertype
