
var dbCon = require('../../config/db.config')

var Faculty = function(faculty) {
  this.id = faculty.id
  this.f_name = faculty.f_name
}
// get all faculty
Faculty.getAllFaculty = (result) => {
  dbCon.query('SELECT * FROM faculty', (err, res) => {
    if(err) {
      console.log('Error while fetching faculty', err)
      result(null, err)
    }else {
      console.log('faculty fetched successfully')
      result(null, res)
    }
  })
}
// get Faculty by id 
Faculty.getFacultyById = (id, result) => {
  dbCon.query('SELECT * FROM faculty WHERE id=?', id, (err, res) => {
    if(err) {
      console.log('Error while fetching faculty by id', err)
      result(null,err)
    }else {
      console.log('faculty fetched by id successfully')
      result(null,res)
    }
  })
}
// insert faculty
Faculty.createNewFaculty = (facultyReqData, result) => {
  dbCon.query('INSERT INTO faculty SET ?', facultyReqData, (err, res) => {
    if(err) {
      console.log('Error while inserting data')
      result(null,err)
    }else {
      console.log('Insert new faculty successfully')
      result(null, res)
    }
  })
}
// update faculty
Faculty.updateFaculty = (id, facultyReqData, result) => {
  dbCon.query('UPDATE faculty SET f_name=? WHERE id=?', [facultyReqData.f_name, id] , (err, res) => {
    if(err) {
      console.log('Error while update data')
      result(null, err)
    } else {
      console.log('Update faculty successfully')
      result(null, res)
    }
  })
}
// delete faculty
Faculty.deleteFaculty = (id, result) => {
  dbCon.query('DELETE FROM faculty WHERE id=?', [id], (err, res) => {
    if(err) {
      console.log('Error while deleting faculty')
      result(null, err)
    }else {
      console.log('Delete faculty successfully')
      result(null, res)
    }
  })
}
module.exports = Faculty
