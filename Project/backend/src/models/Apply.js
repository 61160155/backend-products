
var dbCon = require('../../config/db.config')

var Apply = function(member) {
  this.id = member.id
  this.m_Username = member.m_Username
  this.m_Email = member.m_Email
  this.m_Password = member.m_Password
  this.Repassword = member.Repassword
  this.m_Birthdate = member.m_Birthdate
  this.m_usertype = member.m_usertype
  this.m_faculty = member.m_faculty
  this.m_status = member.m_status
}
// get all Applys
Apply.getAllApply = (result) => {
  dbCon.query('SELECT * FROM member', (err, res) => {
    if(err) {
      console.log('Error while fetching member', err)
      result(null, err)
    }else {
      console.log('member fetched successfully')
      result(null, res)
    }
  })
}
// get Apply by id 
Apply.getApplyById = (id, result) => {
  dbCon.query('SELECT * FROM member WHERE id=?', id, (err, res) => {
    if(err) {
      console.log('Error while fetching member by id', err)
      result(null,err)
    }else {
      console.log('member fetched by id successfully')
      result(null,res)
    }
  })
}
// insert member
Apply.createNewApply = (memberReqData, result) => {
  dbCon.query('INSERT INTO member SET ?', memberReqData, (err, res) => {
    if(err) {
      console.log('Error while inserting data')
      result(null,err)
    }else {
      console.log('Insert new member successfully')
      result(null, res)
    }
  })
}
// update Apply
Apply.updateApply = (id, memberReqData, result) => {
  dbCon.query('UPDATE member SET m_Username=?, m_Email=?, m_Password=?, Repassword=?, m_Birthdate=?, m_usertype=?, m_faculty=?, m_status=? WHERE id=?', [memberReqData.m_Username,memberReqData.m_Email,memberReqData.m_Password,memberReqData.Repassword,memberReqData.m_Birthdate,memberReqData.m_usertype,memberReqData.m_faculty, memberReqData.m_status ,id] , (err, res) => {
    if(err) {
      console.log('Error while update data')
      result(null, err)
    } else {
      console.log('Update member successfully')
      result(null, res)
    }
  })
}
// delete Apply
Apply.deleteApply = (id, result) => {
  dbCon.query('DELETE FROM member WHERE id=?', [id], (err, res) => {
    if(err) {
      console.log('Error while deleting member')
      result(null, err)
    }else {
      console.log('Delete member successfully')
      result(null, res)
    }
  })
}
module.exports = Apply
