const express = require('express')
const router = express.Router()

const usertypeController = require('../controllers/UsertypeController')
// get all Usertype
router.get('/', usertypeController.getAllUsertype)
// get Usertype by id
router.get('/:id', usertypeController.getUsertypeById)
// create new Usertype
router.post('/', usertypeController.createNewUsertype)
// update Usertype
router.put('/:id', usertypeController.updateUsertype)
// delete Usertype
router.delete('/:id', usertypeController.deleteUsertype)

module.exports = router