const express = require('express')
const router = express.Router()

const applyController = require('../controllers/ApplyController')
// get all applys
router.get('/', applyController.getAllApply)
// get apply by id
router.get('/:id', applyController.getApplyById)
// create new apply
router.post('/', applyController.createNewApply)
// update apply
router.put('/:id', applyController.updateApply)
// delete apply
router.delete('/:id', applyController.deleteApply)

module.exports = router