const express = require('express')
const router = express.Router()

const facultyController = require('../controllers/FacultyController')
// get all faculty
router.get('/', facultyController.getAllFaculty)
// get faculty by id
router.get('/:id', facultyController.getFacultyById)
// create new faculty
router.post('/', facultyController.createNewFaculty)
// update faculty
router.put('/:id', facultyController.updateFaculty)
// delete faculty
router.delete('/:id', facultyController.deleteFaculty)

module.exports = router