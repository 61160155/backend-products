const bodyParser = require('body-parser')
const express = require('express')
const app = express()
const port = process.env.PORT || 8081
const cors = require('cors')
app.use(bodyParser.urlencoded({extended: false}))
app.use(cors())
app.use(bodyParser.json())

app.get('/', (req, res) => {
  res.send('Welcome to Informatics Result System')
})

// import routes
// const userRouter = require('./src/routes/user')
const applyRouter = require('./src/routes/apply')
const usertypeRouter = require('./src/routes/usertype')
const facultyRouter = require('./src/routes/faculty')


// create routes
// app.use('/users', userRouter)
app.use('/apply', applyRouter)
app.use('/usertype', usertypeRouter)
app.use('/faculty', facultyRouter)


app.listen(port, ()=> {
  console.log('server is running...')
})