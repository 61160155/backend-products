const mysql = require('mysql')

const dbConn = mysql.createConnection( {
  host: 'localhost',
  user: 'root',
  password: '',
  database: 'apply'
})
dbConn.connect(function (error) {
  if(error) throw error;
  console.log('Connected database')
})
module.exports = dbConn;